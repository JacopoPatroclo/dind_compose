FROM docker:latest

RUN apk --update add --no-cache python3 python3-dev py-pip libffi-dev openssl-dev gcc libc-dev make curl openssh-client && \
    pip install docker-compose && \
    docker-compose --version

COPY entry.sh /usr/local/bin/

ENTRYPOINT ["entry.sh"]