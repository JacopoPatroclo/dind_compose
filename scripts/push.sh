#!/usr/bin/env bash
set -e

docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY

docker push registry.gitlab.com/jacopopatroclo/dind_compose